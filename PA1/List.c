//-----
//List.c
//-------
#include<stdio.h>
#include<stdlib.h>
#include "List.h"

// Constructors-Destructors ---------------------------------------------------

// newNode()
// Returns reference to new Node object. Initializes next and data fields.
// Private.
Node newNode(int node_data){
   Node N = malloc(sizeof(NodeObj));
   N->data = node_data;
   N->next = NULL;
   return(N);
}
// freeNode()
// Frees heap memory pointed to by *pL, sets *pL to NULL.
// Private.
void freeNode(Node* pL){
   if( pL!=NULL && *pL!=NULL ){
      free(*pL);
      *pL = NULL;
   }
}

// newList()
// Returns reference to new empty List object.
List newList(void){
   List L;
   L = malloc(sizeof(ListObj));
   L->front = L->back = NULL;
   L->length = 0;
   return(L);
}


// freeList()
// Frees all heap memory associated with List *pL, and sets *pL to NULL.S
void freeList(List* pL){
   if(pL!=NULL && *pL!=NULL) {
      while( !isEmpty(*pL) ) {
         Dequeue(*pL);
      }
      free(*pL);
      *pL = NULL;
   }
}

int length(List L){
  return List->size;
}

int frontValue(List L){
  if(List->size == 0){
    return NULL;
  }
  return List->head->data;
}
int equals(List A, List B){

}
